import CryptoJS from 'crypto-js'

function wordToByteArray (word, length) {
    const ba = []
    const xFF = 0xFF
    if (length > 0) ba.push(word >>> 24)
    if (length > 1) ba.push((word >>> 16) & xFF)
    if (length > 2) ba.push((word >>> 8) & xFF)
    if (length > 3) ba.push(word & xFF)
  
    return ba
  }
  
  function wordArrayToByteArray (wordArray, length) {
    if (wordArray.hasOwnProperty('sigBytes') && wordArray.hasOwnProperty('words')) {
      length = wordArray.sigBytes
      wordArray = wordArray.words
    }
  
    const result = []
    let bytes
    let i = 0
    while (length > 0) {
      bytes = wordToByteArray(wordArray[i], Math.min(4, length))
      length -= bytes.length
      result.push(bytes)
      i++
    }
    return [].concat.apply([], result)
  }
  
  const randomB = (size, cb) => {
    if (size > 65536) throw new Error('requested too many random bytes')
  
    let rawBytes
  
    if (size > 0) {
      const words = CryptoJS.lib.WordArray.random(size)
      const randoms = wordArrayToByteArray(words, size)
      rawBytes = new global.Uint8Array(randoms)
    }
  
    const bytes = Buffer.from(rawBytes.buffer)
  
    if (cb) {
      cb(bytes)
    }
    return bytes
  }
  
  var randomBytes = randomB

  exports.randomBytes = exports.rng = exports.pseudoRandomBytes = exports.prng = randomBytes
  
  // This may need to go elsewhere
  if (typeof window === 'object') {
    if (!window.crypto) window.crypto = {}
    if (!window.crypto.getRandomValues) {
      window.crypto.getRandomValues = function getRandomValues (arr) {
        let orig = arr
        if (arr.byteLength != arr.length) {
          // Get access to the underlying raw bytes
          arr = new Uint8Array(arr.buffer)
        }
        const bytes = randomBytes(arr.length)
        for (var i = 0; i < bytes.length; i++) {
          arr[i] = bytes[i]
        }
  
        return orig
      }
    }
  }

exports.createHash = exports.Hash = require('create-hash')
exports.createHmac = exports.Hmac = require('create-hmac')